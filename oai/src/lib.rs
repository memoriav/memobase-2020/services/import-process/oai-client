pub mod err {
    use roxmltree::Node;
    use std::borrow::Cow;
    use std::convert::From;
    use std::fmt::Display;
    use url::Url;

    struct OaiParams {
        pub url: String,
        pub verb: String,
        pub identifier: String,
        pub set: String,
        pub from: String,
        pub until: String,
        pub metadata_prefix: String,
        pub resumption_token: String,
    }

    impl From<Url> for OaiParams {
        fn from(value: Url) -> Self {
            let url = value.to_string();
            let mut oai_params = OaiParams {
                url,
                verb: String::from("<unknown>"),
                identifier: String::from("<unknown>"),
                set: String::from("<unknown>"),
                from: String::from("<unknown>"),
                until: String::from("<unknown>"),
                metadata_prefix: String::from("<unknown>"),
                resumption_token: String::from("<unknown>"),
            };

            for (k, v) in value.query_pairs() {
                match k {
                    Cow::Borrowed("verb") => oai_params.verb = v.to_string(),
                    Cow::Borrowed("identifier") => oai_params.identifier = v.to_string(),
                    Cow::Borrowed("set") => oai_params.set = v.to_string(),
                    Cow::Borrowed("from") => oai_params.from = v.to_string(),
                    Cow::Borrowed("until") => oai_params.until = v.to_string(),
                    Cow::Borrowed("metadataPrefix") => oai_params.metadata_prefix = v.to_string(),
                    Cow::Borrowed("resumptionToken") => oai_params.resumption_token = v.to_string(),
                    _ => (),
                }
            }
            oai_params
        }
    }

    /// The error enum
    #[derive(Debug)]
    pub enum Error {
        InvalidArgument(String),
        InvalidResponse(String),
        Internal(String),
        NotFound(String),
        BadArgument(String),
        BadResumptionToken(String),
        BadVerb(String),
        CannotDisseminateFormat(String),
        IdDoesNotExist(String),
        NoRecordsMatch(String, String, String, String),
        NoMetadataFormats,
        NoSetHierarchy,
    }

    impl Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            use Error::*;
            match self {
                InvalidArgument(ref s) => write!(f, "Invalid argument: '{}'", s),
                InvalidResponse(ref s) => write!(f, "Invalid response: '{}'", s),
                Internal(ref s) => write!(f, "Internal error: '{}'", s),
                NotFound(ref s) => write!(f, "Not found: '{}'", s),
                BadArgument(ref url) => write!(f, "The request ({}) includes illegal arguments, is missing required arguments, includes a repeated argument, or values for arguments have an illegal syntax (error code: badArgument).", url),
                BadResumptionToken(ref token) => write!(f, "The value of the resumptionToken ({}) argument is invalid or expired (error code: badResumptionToken).", token),
                BadVerb(ref verb) => write!(f, "Value of the verb argument ({}) is not a legal OAI-PMH verb, the verb argument is missing, or the verb argument is repeated (error code: badVerb).", verb),
                CannotDisseminateFormat(ref format) => write!(f, "The metadata format identified by the value given for the metadataPrefix ({}) argument is not supported by the item or by the repository (error code: cannotDisseminateFormat).", format),
                IdDoesNotExist(ref id) => write!(f, "The value of the identifier ({}) argument is unknown or illegal in this repository (error code: idDoesNotExist).", id),
                NoRecordsMatch(ref from, ref until, ref set, ref metadata_prefix) => write!(f, "The combination of the values of the from ({}), until ({}), set ({}) and metadataPrefix ({}) arguments results in an empty list (error code: noRecordsMatch).", from, until, set, metadata_prefix),
                NoMetadataFormats => write!(f, "There are no metadata formats available for the specified item (error code: noMetadataFormats)"),
                NoSetHierarchy => write!(f, "The repository does not support sets (error code: noSetHierarchy)."),
            }
        }
    }

    impl std::error::Error for Error {}

    pub fn invalid_argument<T: Display>(t: T) -> Error {
        Error::InvalidArgument(t.to_string())
    }

    pub fn invalid_response<T: Display>(t: T) -> Error {
        Error::InvalidResponse(t.to_string())
    }

    pub fn internal<T: Display>(t: T) -> Error {
        Error::Internal(t.to_string())
    }

    pub fn not_found<T: Display>(t: T) -> Error {
        Error::NotFound(t.to_string())
    }

    pub fn convert_oai_error(xml_root_node: Node, url: Url) -> Result<()> {
        let oai_params: OaiParams = url.into();
        let error_node = xml_root_node.children().find(|c| c.has_tag_name("error"));
        if let Some(error) = error_node {
            match error.attribute("code") {
                Some("badArgument") => Err(Error::BadArgument(oai_params.url)),
                Some("badResumptionToken") => {
                    Err(Error::BadResumptionToken(oai_params.resumption_token))
                }
                Some("badVerb") => Err(Error::BadVerb(oai_params.verb)),
                Some("cannotDisseminateFormat") => {
                    Err(Error::CannotDisseminateFormat(oai_params.metadata_prefix))
                }
                Some("idDoesNotExist") => Err(Error::IdDoesNotExist(oai_params.identifier)),
                Some("noRecordsMatch") => Err(Error::NoRecordsMatch(
                    oai_params.from,
                    oai_params.until,
                    oai_params.set,
                    oai_params.metadata_prefix,
                )),
                Some("noMetadataFormats") => Err(Error::NoMetadataFormats),
                Some("noSetHierarchy") => Err(Error::NoSetHierarchy),
                Some(e) => Err(Error::InvalidResponse(format!(
                    "OAI server responded with an unrecognised error code: {}",
                    e
                ))),
                _ => Err(Error::InvalidResponse(
                    "OAI server responded with an unrecognised error".to_string(),
                )),
            }
        } else {
            Ok(())
        }
    }

    /// Result wrapper: `Result<T, Error>`
    pub type Result<T> = std::result::Result<T, Error>;

    #[cfg(test)]
    mod test {
        use super::*;
        use std::{fs, path::PathBuf};

        #[test]
        fn test_convert_oai_error_to_error() {
            let url = Url::parse("https://example.com").unwrap();
            let dir =
                PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("resources/test/input/error.xml");
            let error_obj = fs::read_to_string(dir).unwrap();
            let doc = roxmltree::Document::parse(&error_obj).unwrap();
            assert!(convert_oai_error(doc.root_element(), url).is_err())
        }
    }
}

use std::sync::Arc;

#[derive(Clone)]
pub struct Client {
    pub inner: Arc<reqwest::Client>,
    pub base_url: url::Url,
}

impl Client {
    pub fn new<T: std::borrow::Borrow<str>>(url: T) -> err::Result<Self> {
        let mut header_map = reqwest::header::HeaderMap::new();
        header_map.insert(
            "Accept-Encoding",
            reqwest::header::HeaderValue::from_static("gzip, deflate, br, identity"),
        );
        header_map.insert("Content-Type", reqwest::header::HeaderValue::from_static("text/xml"));
        let client = reqwest::Client::builder()
            .default_headers(header_map)
            .gzip(true)
            .brotli(true)
            .deflate(true)
            .build()
            .unwrap();
        Ok(Client {
            inner: Arc::new(client),
            base_url: url.borrow().parse().map_err(err::invalid_argument)?,
        })
    }
}

pub mod metadata {

    use crate::err;
    use crate::ext::NodeExt;
    use log::debug;
    use std::str;

    #[derive(Debug, Clone, serde::Serialize)]
    pub struct Header {
        pub identifier: String,
        pub datestamp: chrono::DateTime<chrono::Utc>,
        pub set_spec: Vec<String>,
        pub deleted: bool,
    }

    #[derive(Debug, Clone, serde::Serialize)]
    pub struct Record {
        pub header: Header,
        pub full_record: String,
    }

    pub fn extract_record(record: &roxmltree::Node) -> err::Result<String> {
        let record_range = record.range();
        let record_content = record.document().input_text().bytes();
        debug!("{} bytes of record data found", record_content.len());
        let child_node_content = record_content
            .skip(record_range.start)
            .take(record_range.end - record_range.start)
            .collect::<Vec<u8>>();
        Ok(str::from_utf8(&child_node_content).unwrap().to_string())
    }

    // Dirty hack for documents who don't have the XSI namespace definition on mets tags
    // TODO: Remove if fixed on provider side
    pub fn has_xsi_namespace(record: &roxmltree::Node) -> bool {
        let test = record
            .find_first_child("metadata")
            .and_then(|metadata_node| metadata_node.find_first_child("mets"))
            .ok();
        test.map(|mets| {
            let mut attributes = mets.attributes();
            attributes.any(|e| e.name() == "schemaLocation")
                && !attributes.any(|e| e.name() == "xsi")
        })
        .unwrap_or(false)
    }

    // Dirty hack for documents who don't have the XSI namespace definition on mets tags
    // TODO: Remove if fixed on provider side
    pub fn add_xsi_namespace(record: String) -> String {
        record.replacen(
            "<mets:mets ",
            r#"<mets:mets xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" "#,
            1,
        )
    }
}

pub mod get_record {

    use chrono::prelude::*;
    use log::{debug, warn};
    use url::Url;

    use crate::{
        err,
        ext::NodeExt,
        metadata::{self, extract_record},
        Client,
    };

    #[derive(Debug, serde::Serialize)]
    pub struct GetRecord {
        pub response_date: DateTime<Utc>,
        pub record: metadata::Record,
    }

    impl GetRecord {
        fn build(identifier: &str, text: String, url: Url) -> err::Result<Self> {
            let doc = roxmltree::Document::parse(&text).map_err(err::invalid_response)?;

            let root = doc.root_element();

            let response_date = root.find_first_child_parsed_text("responseDate")?;

            err::convert_oai_error(root, url)?;

            let get_record = root.find_first_child("GetRecord")?;

            get_record
                .children()
                .filter(|x| x.has_tag_name("record"))
                .flat_map(|record| -> err::Result<GetRecord> {
                    let header = record.find_first_child("header")?;
                    let identifier: String =
                        header.find_first_child_parsed_text("identifier").map_err(|e| {
                            warn!("{}", e);
                            e
                        })?;

                    let datestamp: DateTime<Utc> = *header
                        .find_first_child("datestamp")
                        .ok()
                        .and_then(|ds| ds.text())
                        .and_then(|ds| TryInto::<super::ext::DateWrapper<Utc>>::try_into(ds).ok())
                        .unwrap();

                    let set_spec: Vec<String> = header
                        .children()
                        .filter(|y| y.has_tag_name("setSpec"))
                        .flat_map(|x| x.text().map(String::from))
                        .collect();
                    let deleted: bool = header.attribute("status").iter().any(|v| v == &"deleted");

                    let header = metadata::Header { identifier, datestamp, set_spec, deleted };
                    let record = extract_record(&record)?;
                    Ok(GetRecord {
                        response_date,
                        record: metadata::Record { header, full_record: record },
                    })
                })
                .next()
                .ok_or_else(|| err::not_found(identifier))
        }
    }

    impl Client {
        pub async fn get_record(
            &self,
            identifier: &str,
            metadata_prefix: &Option<String>,
        ) -> err::Result<GetRecord> {
            #[derive(serde::Serialize)]
            struct VerbedParams<'b> {
                identifier: &'b str,
                verb: &'static str,
                #[serde(rename = "metadataPrefix")]
                metadata_prefix: &'b Option<String>,
            }

            let mut url = self.base_url.clone();

            let params = VerbedParams { identifier, verb: "GetRecord", metadata_prefix };

            url.set_query(Some(&serde_urlencoded::to_string(&params).map_err(err::internal)?));

            debug!("Getting document from {}", url);

            let body = self
                .inner
                .get(url.clone())
                .send()
                .await
                .map_err(err::internal)?
                .text()
                .await
                .map_err(err::internal)?;

            GetRecord::build(identifier, body, url)
        }
    }

    #[cfg(test)]
    mod tests {
        use std::{fs, path::PathBuf};

        use super::*;

        fn test_parse_oai_record(path_left: PathBuf, path_right: PathBuf) -> (String, String) {
            let re = regex::Regex::new(r"\s+").unwrap();
            let url = url::Url::parse("https://example.com").unwrap();
            let id = "some_id";
            let input_obj = fs::read_to_string(path_left).unwrap();
            let get_record = GetRecord::build(id, input_obj, url).unwrap();
            let output_obj = fs::read_to_string(path_right).unwrap();
            (
                re.replace_all(&get_record.record.full_record, "").to_string(),
                re.replace_all(&output_obj, "").to_string(),
            )
        }

        #[test]
        fn test_parse_scope_record() {
            let input_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/input/getrecord_scope.xml");
            let output_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/output/getrecord_scope.xml");
            let result = test_parse_oai_record(input_file, output_file);
            assert_eq!(result.0, result.1);
        }

        #[test]
        fn test_parse_marc_record() {
            let input_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/input/getrecord_marc.xml");
            let output_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/output/getrecord_marc.xml");
            let result = test_parse_oai_record(input_file, output_file);
            assert_eq!(result.0, result.1);
        }
    }
}

pub mod list_records {

    use chrono::prelude::*;
    use log::{debug, warn};
    use url::Url;

    use crate::{err, ext::*, metadata, Client};

    #[derive(Debug, serde::Serialize)]
    pub struct ResumptionToken {
        value: String,
        complete_list_size: Option<u64>,
        cursor: Option<u64>,
    }

    #[derive(Debug, serde::Serialize)]
    pub struct ListRecords {
        pub response_date: DateTime<Utc>,
        pub records: Vec<metadata::Record>,
        pub resumption_token: Option<ResumptionToken>,
    }

    #[derive(Default, Debug, serde::Serialize)]
    pub struct Params {
        pub from: Option<DateTime<Utc>>,
        pub until: Option<DateTime<Utc>>,
        pub set: Option<String>,
        #[serde(rename = "metadataPrefix")]
        pub metadata_prefix: Option<String>,
    }

    impl ListRecords {
        pub fn has_next(&self) -> bool {
            self.resumption_token.is_some()
        }

        pub async fn get_next(self, client: &Client) -> err::Result<ListRecords> {
            if let Some(rt) = self.resumption_token.as_ref().map(|x| x.value.clone()) {
                Ok(client.list_records_from_resumption_token(rt).await?)
            } else {
                Err(err::internal("No more results"))
            }
        }

        fn build(text: String, url: Url) -> err::Result<ListRecords> {
            let doc = roxmltree::Document::parse(&text).map_err(err::invalid_response)?;

            let root = doc.root_element();

            let response_date = root.find_first_child_parsed_text("responseDate")?;

            err::convert_oai_error(root, url)?;

            let list_records = root.find_first_child("ListRecords")?;

            let resumption_token =
                list_records.find_first_child("resumptionToken").ok().and_then(|x| {
                    x.text().map(|t| ResumptionToken {
                        value: t.to_owned(),
                        complete_list_size: x
                            .attribute("completeListSize")
                            .and_then(|x| x.parse().ok()),
                        cursor: x.attribute("cursor").and_then(|x| x.parse().ok()),
                    })
                });

            let records = list_records
                .children()
                .filter(|x| x.has_tag_name("record"))
                .flat_map(|record| -> err::Result<metadata::Record> {
                    let header = record.find_first_child("header")?;
                    let identifier: String =
                        header.find_first_child_parsed_text("identifier").map_err(|e| {
                            warn!("{}", e);
                            e
                        })?;

                    let datestamp: DateTime<Utc> = *header
                        .find_first_child("datestamp")
                        .ok()
                        .and_then(|ds| ds.text())
                        .and_then(|ds| TryInto::<super::ext::DateWrapper<Utc>>::try_into(ds).ok())
                        .unwrap();

                    let set_spec: Vec<String> = header
                        .children()
                        .filter(|y| y.has_tag_name("setSpec"))
                        .flat_map(|x| x.text().map(String::from))
                        .collect();
                    let deleted: bool = header.attribute("status").iter().any(|v| v == &"deleted");
                    let header = metadata::Header { identifier, datestamp, set_spec, deleted };

                    let mut record_as_text = metadata::extract_record(&record)?;

                    // Dirty hack for documents who don't have the XSI namespace definition on mets tags
                    // TODO: Remove if fixed on provider side
                    if metadata::has_xsi_namespace(&record) {
                        record_as_text = metadata::add_xsi_namespace(record_as_text);
                    }

                    Ok(metadata::Record { header, full_record: record_as_text })
                })
                .collect::<Vec<metadata::Record>>();
            debug!("Got {} records", records.len());

            Ok(ListRecords { response_date, records, resumption_token })
        }
    }

    impl Client {
        pub async fn list_records(&self, params: Option<Params>) -> err::Result<ListRecords> {
            let params = params.unwrap_or_default();

            #[derive(serde::Serialize)]
            struct VerbedParams {
                #[serde(flatten)]
                params: Params,
                verb: &'static str,
            }

            let mut url = self.base_url.clone();

            let params = VerbedParams { params, verb: "ListRecords" };

            url.set_query(Some(&serde_urlencoded::to_string(&params).map_err(err::internal)?));

            debug!("Getting documents from {}", url);

            let body = self
                .inner
                .get(url.clone())
                .send()
                .await
                .map_err(err::internal)?
                .text()
                .await
                .map_err(err::internal)?;

            ListRecords::build(body, url)
        }

        pub async fn list_records_from_resumption_token(
            &self,
            resumption_token: String,
        ) -> err::Result<ListRecords> {
            #[derive(serde::Serialize)]
            struct VerbedParams {
                #[serde(rename = "resumptionToken")]
                resumption_token: String,
                verb: &'static str,
            }

            let mut url = self.base_url.clone();

            let params = VerbedParams { resumption_token, verb: "ListRecords" };

            url.set_query(Some(&serde_urlencoded::to_string(&params).map_err(err::internal)?));

            debug!("Getting more document from {}", url);

            let body = self
                .inner
                .get(url.clone())
                .send()
                .await
                .map_err(err::internal)?
                .text()
                .await
                .map_err(err::internal)?;

            ListRecords::build(body, url)
        }

        pub async fn list_records_all(&self, params: Option<Params>) -> err::Result<ListRecords> {
            let mut list_records = self.list_records(params).await?;
            while list_records.has_next() {
                let mut prev_records = list_records.records.drain(..).collect::<Vec<_>>();

                list_records = list_records.get_next(self).await?;

                prev_records.append(&mut list_records.records);

                list_records.records = prev_records;
            }

            Ok(list_records)
        }
    }

    #[cfg(test)]
    mod tests {
        use std::{fs, path::PathBuf};

        use super::*;

        fn test_parse_oai_records<'a>(path_left: PathBuf, path_right: PathBuf) -> (String, String) {
            let re = regex::Regex::new(r"\s+").unwrap();
            let url = url::Url::parse("https://example.com").unwrap();
            let input_obj = fs::read_to_string(path_left).unwrap();
            let list_records = ListRecords::build(input_obj, url).unwrap();
            let output_obj = fs::read_to_string(path_right).unwrap();
            (
                re.replace_all(&list_records.records.first().unwrap().full_record, "").to_string(),
                re.replace_all(&output_obj, "").to_string(),
            )
        }

        #[test]
        fn test_parse_oai_dc_records() {
            let input_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/input/listrecords_oaidc.xml");
            let output_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/output/listrecords_oaidc_first_record.xml");
            let result = test_parse_oai_records(input_file, output_file);

            assert_eq!(result.0, result.1)
        }

        #[test]
        fn test_parse_mets_records() {
            let input_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/input/listrecords_mets.xml");
            let output_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/output/listrecords_mets_first_record.xml");
            let result = test_parse_oai_records(input_file, output_file);

            assert_eq!(result.0, result.1)
        }

        #[test]
        fn test_parse_marc_records() {
            let input_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/input/listrecords_marc.xml");
            let output_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
                .join("resources/test/output/listrecords_marc_first_record.xml");
            let result = test_parse_oai_records(input_file, output_file);

            assert_eq!(result.0, result.1)
        }
    }
}

pub mod ext {

    pub use chrono;
    pub use roxmltree;

    use std::ops::Deref;

    use chrono::{DateTime, NaiveDate, NaiveDateTime, TimeZone, Utc};
    use roxmltree::{ExpandedName, Node};

    pub(crate) trait NodeExt<'a, 'b, 'd>: Sized {
        fn find_first_child<I: Into<ExpandedName<'a, 'b>>>(
            &self,
            tag_name: I,
        ) -> crate::err::Result<Node<'a, 'd>>;
        fn find_first_child_parsed_text<P: std::str::FromStr, I: Into<ExpandedName<'a, 'b>>>(
            &'a self,
            tag_name: I,
        ) -> crate::err::Result<P>;
    }

    impl<'a, 'b, 'd: 'a> NodeExt<'a, 'b, 'd> for Node<'a, 'd> {
        fn find_first_child<I: Into<ExpandedName<'a, 'b>>>(
            &self,
            tag_name: I,
        ) -> crate::err::Result<Node<'a, 'd>> {
            let tag_name = tag_name.into();
            self.children()
                .find(|x| x.has_tag_name(tag_name))
                .ok_or_else(|| crate::err::internal(format!("No such tag: {}", tag_name.name())))
        }

        fn find_first_child_parsed_text<P: std::str::FromStr, I: Into<ExpandedName<'a, 'b>>>(
            &'a self,
            tag_name: I,
        ) -> crate::err::Result<P> {
            let tag_name = tag_name.into();
            self.find_first_child(tag_name)?
                .text()
                .and_then(|x| x.parse().ok())
                .ok_or_else(|| crate::err::internal(format!("No text for: {}", tag_name.name())))
        }
    }

    pub struct DateWrapper<T: TimeZone>(DateTime<T>);

    impl TryFrom<&str> for DateWrapper<Utc> {
        type Error = &'static str;
        fn try_from(value: &str) -> Result<Self, Self::Error> {
            if let Ok(dt) = NaiveDateTime::parse_from_str(value, "%FT%TZ") {
                Ok(DateWrapper(Utc.from_utc_datetime(&dt)))
            } else if let Ok(d) = NaiveDate::parse_from_str(value, "%F") {
                if let Some(dt) = d.and_hms_opt(0, 0, 0) {
                    if let chrono::offset::LocalResult::Single(dtz) = dt.and_local_timezone(Utc) {
                        Ok(DateWrapper(dtz))
                    } else {
                        Err("Conversion to datetime failed")
                    }
                } else {
                    Err("Conversion to datetime failed")
                }
            } else {
                Err("Invalid date format")
            }
        }
    }

    impl Deref for DateWrapper<Utc> {
        type Target = DateTime<Utc>;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        #[test]
        fn convert_datestamp() {
            let date_str = "2022-01-02";
            let date: Result<DateWrapper<Utc>, _> = date_str.try_into();
            assert!(date.is_ok())
        }

        #[test]
        fn convert_datetimestamp() {
            let datetime_str = "2022-01-02T12:01:59Z";
            let date: Result<DateWrapper<Utc>, _> = datetime_str.try_into();
            assert!(date.is_ok())
        }
    }
}
