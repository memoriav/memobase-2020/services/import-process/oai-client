# Memobase OAI Client

Fetches and normalises metadata documents from OAI-PMH servers in order to
import them into Memobase.

## GetRecord and ListRecords

The OAI client currently supports two OAI verbs: `GetRecord` and `ListRecords`.

* __`GetRecord`__: Fetches a single document per request. This approach is used
  for collections where `document`, `document_list` or both is defined in the
  mapping.
* __`ListRecords`__: Fetches a list of documents per request. Used for
  collections where neither `document` or `document_list` is explicitly set in
  the mapping.

## Configuration

The application relies on three different sets of configuration:

* More or less static general application settings, e.g. the name of the Kafka
  bootstrap servers. These settings are defined via environmental variables in
  order to facilitate the reuse of already existing `ConfigMap`s in
  Kubernetes. 
* Ephemeral settings which are prone to change from job to job like the
  collection ids which should be fetched. This sort of settings are declared
  via command-line arguments.
* Finally a mapping which relates Memobase collection IDs to OAI-PMH
  endpoints. The mapping is mounted as a `ConfigMap`;

### General Application Settings

* `APP_VERSION`: Application version
* `LOG_LEVEL`: Log level (`error`, `warn`, `info` or `debug`)
* `MAPPING_FILE_PATH`: Path to file containing the mapping (see below)
* `DOCUMENT_LIST_PATH`: Path to directory containing additional document lists (see below)
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap servers
* `KAFKA_OUTPUT_TOPIC`: Name of Kafka output topic
* `KAFKA_REPORTS_TOPIC`: Name of Kafka reports topic
* `KAFKA_SECURITY_PROTOCOL`: Protocol used to communicate with brokers. Valid values are: `plaintext`, `ssl`, `sasl_plaintext`, `sasl_ssl`
* `KAFKA_SSL_CA_LOCATION`: File or directory path to CA certificate(s) for verifying the broker's key
* `KAFKA_SSL_KEY_LOCATION`: Path to client's private key (PEM) used for authentication
* `KAFKA_SSL_CERTIFICATE_LOCATION`: Path to client's public key (PEM) used for authentication
* `KAFKA_ENABLE_SSL_CERTIFICATE_VERIFICATION`: Enable OpenSSL's builtin broker (server) certificate verification. `true` or `false`
* `KAFKA_PRODUCER_CONFIGURATIONS`: Custom Kafka producer configurations in the
  form of `key1=value1,key2=value2,...`
* `REPORTING_STEP_NAME`: Name of application as used in reports

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Client certificate file in PEM format at the path defined in `KAFKA_SSL_CERTIFICATE_LOCATION`
* Client certificate key file in PEM format and at the path defined in `KAFKA_SSL_KEY_LOCATION`
* CA certificates in PEM format at the path defined in `KAFKA_SSL_CA_LOCATION`

### Memobase collection IDs

If the application is run without any arguments, all collections defined in
the mapping are processed. On the other hand, if collection ids are provided
as arguments (i.e. collection ids as used in Memobase), only these are
processed. If a collection id can't be found in the mapping, this collection
is ignored.

### ID Mapping

This file serves three purposes.

1) It maps Memobase collection IDs to remote metadata sets. At the very least, each collection id maps to a string with the base URL of the OAI repository.

2) As a whole, it is a comprehensive list of all collections which will be
imported if the set is not restricted by the respective command-line argument.

3) For each collection, specific options can be defined. In this case, a collection id maps to an object with the following fields:
    - `base_url` (obligatory): The base URL of the OAI repository
    - `set` (optional): A OAI set to be fetched
    - `metadata` (optional): The metadata format of the response. Possible values are `marc21`, `xoai` or `oai_dc` (the default). Furthermore, there is the special "metadata" value `f+f`, which stands for the special processing of F+F collections.
    - `document` (optional): The identifier of a single document
    - `document_list` (optional): The filename of a document list (the file is expected in `DOCUMENT_LIST_PATH` (see above))
    - `date_range` (optional): If present, only document in the indicated date ranged are harvested. Possible values are `last_year` (documents modified in the last year), `last_month` (documents modified in the last month), `last_week` (documents modified in the last week), `date - date` (from date to date (inclusive), ` - date` (until date), `date - ` (from date) (_always mind the the form ` - `, i.e. `<space>-<space>`!_). `date` must be in the form of `YYYY-MM-DD`.
    - `xml_record_tag` (optional): Name of the tag containing the metadata (used by service [xml-data-transform](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/xml-data-transform/))
    - `xml_identifier_field_name` (optional): Field name containing the identifier (used by service [xml-data-transform](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/xml-data-transform/))
    - `ignore` (optional): Ignore collection when importing; defaults to `false`

#### Examples

##### Without any additional configs

```json
{
  "xyz-001": "http://example.com/oai"
}
```

##### A set of documents

```json
{
  "xyz-001": {
    "base_url": "http://memory.loc.gov/cgi-bin/oai",
    "set": "physics:hep",
    "metadata": "oai_dc",
    "date_range": "2010-01-02 - "
  }
}
```

##### A single document

```json
{
  "xyz-001": {
    "base_url": "http://memory.loc.gov/cgi-bin/oai",
    "metadata": "oai_dc",
    "document": "oai:example:123-xyz"
  }
}
```

##### Pointer to a document list

```json
{
  "xyz-001": {
    "base_url": "http://memory.loc.gov/cgi-bin/oai",
    "metadata": "oai_dc",
    "document_list": "example.csv"
  }
}
```

### Document lists

If `document_list` is defined in the mapping, a corresponding file needs to be
placed in the directory as defined in `DOCUMENT_LIST_PATH`. The file must
consist of a list of identifiers, a single identifier per line.

The default Dockerfile mounts files which resides in the `document_lists`
directory in the repository to `/app/document_lists`. If you use indeed the
default approach, `DOCUMENT_LIST_PATH` should be set accordingly, i.e. to
`/app/document_lists`.
