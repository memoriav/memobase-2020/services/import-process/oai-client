use anyhow::{bail, Result};
use chrono::{offset::TimeZone, DateTime, Datelike, NaiveDate, NaiveDateTime, Utc, Weekday};

pub fn string_to_date(s: &str, inclusive: bool) -> Result<DateTime<Utc>> {
    let date_fmt = "%F";
    let date_time_fmt = "%FT%TZ";
    if let Ok(d) = NaiveDate::parse_from_str(s, date_fmt) {
        let dt = if inclusive {
            d.and_hms_opt(23, 59, 59)
        } else {
            d.and_hms_opt(0, 0, 0)
        }
        .expect(
            "numbers used for hours, minutes and seconds should be in the respective valid range",
        );
        Ok(Utc.from_utc_datetime(&dt))
    } else if let Ok(d) = NaiveDateTime::parse_from_str(s, date_time_fmt) {
        Ok(Utc.from_utc_datetime(&d))
    } else {
        bail!(
            "Can't understand timestamp {}. Use either YYYY-MM-DD or YYYY-MM-DDThh:mm:ssZ format",
            s
        )
    }
}

pub fn compute_previous_week(today: DateTime<Utc>) -> (DateTime<Utc>, DateTime<Utc>) {
    let week = today.iso_week();
    let (previous_week, year_of_previous_week) = if week.week() == 1 {
        let last_week_of_previous_year = Utc
            .with_ymd_and_hms(today.year() - 1, 12, 31, 0, 0, 0)
            .unwrap()
            .iso_week();
        (last_week_of_previous_year.week(), today.year() - 1)
    } else {
        (week.week() - 1, today.year())
    };
    let first_day_of_previous_week = NaiveDate::from_isoywd_opt(
        year_of_previous_week,
        previous_week,
        Weekday::Mon,
    )
    .expect("Year and week of year should be in valid range")
    .and_hms_opt(0, 0, 0)
    .expect("numbers used for hours, minutes and seconds should be in the respective valid range");
    let first_day_of_previous_week = Utc.from_utc_datetime(&first_day_of_previous_week);
    let last_day_of_previous_week = NaiveDate::from_isoywd_opt(
        year_of_previous_week,
        previous_week,
        Weekday::Sun,
    )
    .expect("Year and week of year should be in valid range")
    .and_hms_opt(23, 59, 59)
    .expect("numbers used for hours, minutes and seconds should be in the respective valid range");
    let last_day_of_previous_week = Utc.from_utc_datetime(&last_day_of_previous_week);
    (first_day_of_previous_week, last_day_of_previous_week)
}

pub fn compute_previous_month(today: DateTime<Utc>) -> (DateTime<Utc>, DateTime<Utc>) {
    let this_month = today.month();
    let this_year = today.year();
    let first_day_of_this_month = Utc
        .with_ymd_and_hms(this_year, this_month, 1, 0, 0, 0)
        .unwrap();
    let (previous_month, year_of_previous_month) = if this_month - 1 < 1 {
        (12, this_year - 1)
    } else {
        (this_month - 1, this_year)
    };
    let first_day_of_previous_month = Utc
        .with_ymd_and_hms(year_of_previous_month, previous_month, 1, 0, 0, 0)
        .unwrap();
    let no_of_days_of_previous_month = first_day_of_this_month
        .signed_duration_since(first_day_of_previous_month)
        .num_days();
    let last_day_of_previous_month = Utc
        .with_ymd_and_hms(
            year_of_previous_month,
            previous_month,
            no_of_days_of_previous_month as u32,
            23,
            59,
            59,
        )
        .unwrap();
    (first_day_of_previous_month, last_day_of_previous_month)
}

pub fn compute_previous_year(today: DateTime<Utc>) -> (DateTime<Utc>, DateTime<Utc>) {
    let previous_year = today.year() - 1;
    let first_day_of_previous_year = Utc.with_ymd_and_hms(previous_year, 1, 1, 0, 0, 0).unwrap();
    let last_day_of_previous_year = Utc
        .with_ymd_and_hms(previous_year, 12, 31, 23, 59, 59)
        .unwrap();
    (first_day_of_previous_year, last_day_of_previous_year)
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono::Timelike;

    #[test]
    fn parse_valid_exclusive_date() {
        let input = "2014-07-08";
        let result = string_to_date(input, false).unwrap();
        let expected = Utc.with_ymd_and_hms(2014, 7, 8, 0, 0, 0).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn parse_valid_inclusive_date() {
        let input = "2014-07-08";
        let result = string_to_date(input, true).unwrap();
        let expected = Utc.with_ymd_and_hms(2014, 7, 8, 23, 59, 59).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn parse_valid_date_time() {
        let input = "2014-07-08T01:02:03Z";
        let result = string_to_date(input, true).unwrap();
        let expected = Utc.with_ymd_and_hms(2014, 7, 8, 1, 2, 3).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn parse_invalid_timestamp() {
        let input = "2014-07-08T01:02:03";
        let result = string_to_date(input, true);
        assert!(result.is_err())
    }

    #[test]
    fn days_last_week() {
        let today = Utc.with_ymd_and_hms(2022, 10, 14, 0, 0, 0).unwrap();
        let (first_day, last_day) = compute_previous_week(today);
        assert_eq!(first_day.date_naive().day(), 3);
        assert_eq!(first_day.time().hour(), 0);
        assert_eq!(last_day.date_naive().day(), 9);
        assert_eq!(last_day.time().hour(), 23);
    }

    #[test]
    fn days_last_week_in_previous_year() {
        let today = Utc.with_ymd_and_hms(2022, 1, 8, 0, 0, 0).unwrap();
        let (first_day, last_day) = compute_previous_week(today);
        assert_eq!(first_day.date_naive().day(), 27);
        assert_eq!(first_day.date_naive().month(), 12);
        assert_eq!(first_day.date_naive().year(), 2021);
        assert_eq!(first_day.time().hour(), 0);
        assert_eq!(last_day.date_naive().day(), 2);
        assert_eq!(last_day.date_naive().month(), 1);
        assert_eq!(last_day.date_naive().year(), 2022);
        assert_eq!(last_day.time().hour(), 23);
    }

    #[test]
    fn days_in_february_of_a_leap_year() {
        let today = Utc.with_ymd_and_hms(2020, 3, 21, 0, 0, 0).unwrap();
        let (first_day, last_day) = compute_previous_month(today);
        assert_eq!(first_day.date_naive().day(), 1);
        assert_eq!(first_day.time().hour(), 0);
        assert_eq!(last_day.date_naive().day(), 29);
        assert_eq!(last_day.date_naive().month(), 2);
        assert_eq!(last_day.date_naive().year(), 2020);
        assert_eq!(last_day.time().hour(), 23);
    }

    #[test]
    fn days_in_december() {
        let today = Utc.with_ymd_and_hms(2022, 1, 15, 0, 0, 0).unwrap();
        let (first_day, last_day) = compute_previous_month(today);
        assert_eq!(first_day.date_naive().day(), 1);
        assert_eq!(first_day.time().hour(), 0);
        assert_eq!(last_day.date_naive().day(), 31);
        assert_eq!(last_day.date_naive().year(), 2021);
        assert_eq!(last_day.time().hour(), 23);
    }

    #[test]
    fn days_in_previous_year() {
        let today = Utc.with_ymd_and_hms(2022, 4, 18, 0, 0, 0).unwrap();
        let (first_day, last_day) = compute_previous_year(today);
        assert_eq!(first_day.date_naive().month(), 1);
        assert_eq!(first_day.time().hour(), 0);
        assert_eq!(last_day.date_naive().day(), 31);
        assert_eq!(last_day.date_naive().year(), 2021);
        assert_eq!(last_day.time().hour(), 23);
    }
}
