use std::{fs::File, io::BufReader, path::Path};

use anyhow::{bail, Result};
use chrono::{DateTime, Utc};
use serde_json::Value;

use crate::date_utils::*;

/// Holds the properties of the OAI collection
pub struct CollectionProperties {
    /// URL pointing to the remote OAI repository
    pub base_url: String,
    /// Optional set name
    pub set: Option<String>,
    /// Optional from date
    pub from_date: Option<DateTime<Utc>>,
    /// Optional until date
    pub until_date: Option<DateTime<Utc>>,
    /// Optional metadata prefix
    pub metadata: Option<String>,
    /// Optional document id
    pub document: Option<String>,
    /// Optional document list (must be in document list path)
    pub document_list: Option<String>,
    /// Name of record tag (used by service xml-data-transform)
    pub xml_record_tag: Option<String>,
    /// Name of field containing identifier (used by service xml-data-transform)
    pub xml_identifier_field_name: Option<String>,
    /// Suspend collection import
    pub ignore: bool,
}

/// Parse a JSON mapping file as a vector of collection properties / collection id tuples for simpler
/// processing
pub fn parse(file_path: &str) -> Result<Vec<(CollectionProperties, String)>> {
    let path = Path::new(file_path);
    let buf_reader = BufReader::new(File::open(path)?);
    let json = serde_json::from_reader(buf_reader)?;
    transform(json)
}

fn transform(json_value: Value) -> Result<Vec<(CollectionProperties, String)>> {
    let json = json_value.as_object();
    if json.is_none() {
        bail!(
            "JSON error. Expects object, instead got:\n{}",
            serde_json::to_string_pretty(&json_value).unwrap()
        );
    }
    let json = json.unwrap();
    let mut collections = vec![];
    for (collection_id, collection_value) in json.into_iter() {
        if collection_value.is_object() {
            let oai_url_parts = collection_value.as_object().unwrap();
            if oai_url_parts.get("base_url").is_none() {
                bail!(
                    "Mapping error: Collection {} needs a field `base_url`!",
                    serde_json::to_string_pretty(&collection_id).unwrap()
                );
            }
            let base_url = oai_url_parts
                .get("base_url")
                .and_then(|v| v.as_str().map(|s| s.to_string()))
                .unwrap();
            let set = oai_url_parts
                .get("set")
                .and_then(|v| v.as_str().map(|s| s.to_string()));
            let (from_date, until_date) =
                if let Some(dr) = oai_url_parts.get("date_range").and_then(|v| v.as_str()) {
                    match dr {
                        "last_year" => {
                            let today = Utc::now();
                            let (fd, ud) = compute_previous_year(today);
                            (Some(fd), Some(ud))
                        }
                        "last_month" => {
                            let today = Utc::now();
                            let (fd, ud) = compute_previous_month(today);
                            (Some(fd), Some(ud))
                        }
                        "last_week" => {
                            let today = Utc::now();
                            let (fd, ud) = compute_previous_week(today);
                            (Some(fd), Some(ud))
                        }
                        s if s.contains(" - ") => {
                            let d = s.splitn(2, " - ").collect::<Vec<&str>>();
                            let fd = if !d[0].is_empty() {
                                Some(
                                    string_to_date(d[0], false)
                                        .unwrap_or_else(|_| panic!("{} is invalid date", d[0])),
                                )
                            } else {
                                None
                            };
                            let ud = if !d[1].is_empty() {
                                Some(
                                    string_to_date(d[1], true)
                                        .unwrap_or_else(|_| panic!("{} is invalid date", d[1])),
                                )
                            } else {
                                None
                            };
                            (fd, ud)
                        }
                        d => bail!("{} not recognised as date", d),
                    }
                } else {
                    (None, None)
                };
            let metadata = oai_url_parts
                .get("metadata")
                .and_then(|v| v.as_str().map(|s| s.to_string()));
            let document = oai_url_parts
                .get("document")
                .and_then(|v| v.as_str().map(|s| s.to_string()));
            let document_list = oai_url_parts
                .get("document_list")
                .and_then(|v| v.as_str().map(|s| s.to_string()));
            let xml_record_tag = oai_url_parts
                .get("xml_record_tag")
                .and_then(|v| v.as_str().map(|s| s.to_string()));
            let xml_identifier_field_name = oai_url_parts
                .get("xml_identifier_field_name")
                .and_then(|v| v.as_str().map(|s| s.to_string()));
            let ignore = oai_url_parts
                .get("ignore")
                .and_then(|v| v.as_bool())
                .unwrap_or(false);
            collections.push((
                CollectionProperties {
                    base_url,
                    set,
                    from_date,
                    until_date,
                    metadata,
                    document,
                    document_list,
                    xml_record_tag,
                    xml_identifier_field_name,
                    ignore,
                },
                collection_id.to_owned(),
            ))
        } else if collection_value.is_string() {
            let base_url = collection_value.as_str().unwrap().to_string();
            collections.push((
                CollectionProperties {
                    base_url,
                    set: None,
                    from_date: None,
                    until_date: None,
                    metadata: None,
                    document: None,
                    document_list: None,
                    xml_record_tag: None,
                    xml_identifier_field_name: None,
                    ignore: false,
                },
                collection_id.to_owned(),
            ))
        } else {
            bail!(
                "JSON error: Expects object or string, instead got:\n{}",
                serde_json::to_string_pretty(&collection_value).unwrap()
            );
        }
    }
    Ok(collections)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn read_valid_file() {
        let mapping = r#"{
                "xyz-001": "https://example.com/oai-pmh",
                "xyz-002": {"base_url": "https://example.com/oai-pmh2", "set": "test:example", "metadata": "marc21", "date_range": "2021-01-01 - 2022-04-01"}
                }"#;
        let value = serde_json::from_str(mapping).unwrap();
        assert_eq!(
            transform(value)
                .unwrap()
                .iter()
                .map(|v| (
                    v.0.base_url.to_owned(),
                    v.0.set.to_owned().unwrap_or("".to_string()),
                    v.0.metadata.to_owned().unwrap_or("".to_string()),
                    v.1.to_owned()
                ))
                .collect::<Vec<(String, String, String, String)>>(),
            vec![
                (
                    "https://example.com/oai-pmh".to_string(),
                    String::from(""),
                    String::from(""),
                    "xyz-001".to_string()
                ),
                (
                    "https://example.com/oai-pmh2".to_string(),
                    "test:example".to_string(),
                    "marc21".to_string(),
                    "xyz-002".to_string(),
                ),
            ]
        );
    }

    #[test]
    fn read_invalid_field() {
        let mapping = r#"{
                "xyz-001": {"set": "test:example"}
                }"#;
        let value = serde_json::from_str(mapping).unwrap();
        assert!(transform(value).is_err());
    }

    #[test]
    fn read_invalid_setting() {
        let mapping = r#"["srf-001", "srf-002"]"#;
        let value = serde_json::from_str(mapping).unwrap();
        assert!(transform(value).is_err());
    }
}
