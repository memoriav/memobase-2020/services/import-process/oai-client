use std::time::Duration;

use anyhow::{bail, Result};
use log::{debug, error, info, warn};
use rdkafka::{
    config::FromClientConfigAndContext,
    message::{Header, OwnedHeaders},
    producer::{BaseRecord, Producer, ProducerContext, ThreadedProducer},
    util::Timeout,
    ClientConfig, ClientContext, Message,
};

use crate::configuration::Configuration;

pub struct CustomProducer<'a> {
    producer: ThreadedProducer<ProducerCallbackLogger>,
    output_topic: &'a str,
}

pub struct CollectionCtxMetadata<'a> {
    pub collection_id: Option<&'a str>,
    pub is_published: Option<bool>,
    pub session_id: &'a str,
    pub xml_record_tag: Option<&'a str>,
    pub xml_identifier_field_name: Option<&'a str>,
    pub step_name: &'a str,
    pub step_version: &'a str,
}

impl<'a> CollectionCtxMetadata<'a> {
    pub fn build_outside_collection(
        session_id: &'a str,
        step_name: &'a str,
        step_version: &'a str,
    ) -> Self {
        Self {
            collection_id: None,
            is_published: None,
            session_id,
            xml_record_tag: None,
            xml_identifier_field_name: None,
            step_name,
            step_version,
        }
    }

    pub fn build_inside_collection(
        collection_id: &'a str,
        is_published: bool,
        session_id: &'a str,
        xml_record_tag: Option<&'a str>,
        xml_identifier_field_name: Option<&'a str>,
        step_name: &'a str,
        step_version: &'a str,
    ) -> Self {
        Self {
            collection_id: Some(collection_id),
            is_published: Some(is_published),
            session_id,
            xml_record_tag,
            xml_identifier_field_name,
            step_name,
            step_version,
        }
    }
}

impl<'a> CollectionCtxMetadata<'a> {
    pub fn build_report_headers(&self) -> OwnedHeaders {
        let institution_id = if let Some(id) = &self.collection_id {
            id.split('-').next()
        } else {
            Some("xxxxx")
        };
        OwnedHeaders::new()
            .insert(Header {
                key: "recordSetId",
                value: Some(self.collection_id.unwrap_or("xxx")),
            })
            .insert(Header {
                key: "institutionId",
                value: institution_id,
            })
            .insert(Header {
                key: "isPublished",
                value: Some(
                    if self.is_published.is_some() && self.is_published.unwrap() {
                        "true"
                    } else {
                        "false"
                    },
                ),
            })
            .insert(Header {
                key: "sessionId",
                value: Some(self.session_id),
            })
    }

    pub fn build_document_headers(&self) -> OwnedHeaders {
        self.build_report_headers()
            .insert(Header {
                key: "xmlRecordTag",
                value: self.xml_record_tag,
            })
            .insert(Header {
                key: "xmlIdentifierFieldName",
                value: self.xml_identifier_field_name,
            })
    }
}

impl<'a> CustomProducer<'a> {
    pub fn new(output_topic: &'a str, config: &Configuration) -> Result<Self> {
        let producer: ThreadedProducer<ProducerCallbackLogger> =
            create_client(config, ProducerCallbackLogger {})?;
        Ok(CustomProducer {
            producer,
            output_topic,
        })
    }

    pub fn send(&self, payload: &str, identifier: &str, headers: OwnedHeaders) -> Result<()> {
        let base_record = BaseRecord::to(&self.output_topic)
            .key(identifier)
            .payload(payload)
            .headers(headers);
        self.producer.send(base_record).map_err(|e| e.0.into())
    }
}

impl<'a> Drop for CustomProducer<'a> {
    fn drop(&mut self) {
        if let Err(e) = self.producer.flush(Timeout::After(Duration::new(20, 0))) {
            error!("Message flushing failed: {}", e);
        } else {
            info!("Remaining messages sent to Kafka");
        }
    }
}

fn create_client_config(app_config: &Configuration) -> Result<ClientConfig> {
    let mut config = ClientConfig::new();
    config.set(
        "bootstrap.servers",
        app_config.kafka_bootstrap_servers.to_string(),
    );
    config.set(
        "security.protocol",
        app_config.kafka_security_protocol.to_string(),
    );
    config.set(
        "ssl.ca.location",
        app_config.kafka_ssl_ca_location.to_string(),
    );
    config.set(
        "ssl.key.location",
        app_config.kafka_ssl_key_location.to_string(),
    );
    config.set(
        "ssl.certificate.location",
        app_config.kafka_ssl_certificate_location.to_string(),
    );
    config.set(
        "enable.ssl.certificate.verification",
        app_config
            .kafka_enable_ssl_certificate_verification
            .to_string(),
    );
    for configuration in &app_config.kafka_producer_configurations {
        let mut key_value = configuration.split('=');
        if let Some(key) = key_value.next() {
            if let Some(value) = key_value.next() {
                config.set(key, value);
            } else {
                bail!("No config value found");
            }
        } else {
            bail!("No config key found");
        }
    }
    Ok(config)
}

pub fn create_client<C, T>(app_config: &Configuration, ctx: C) -> Result<T>
where
    C: ClientContext,
    T: FromClientConfigAndContext<C>,
{
    create_client_config(app_config)?
        .create_with_context(ctx)
        .map_err(|e| e.into())
}

struct ProducerCallbackLogger {}

impl ClientContext for ProducerCallbackLogger {}

impl ProducerContext for ProducerCallbackLogger {
    type DeliveryOpaque = ();

    fn delivery(
        &self,
        delivery_result: &rdkafka::producer::DeliveryResult<'_>,
        _delivery_opaque: Self::DeliveryOpaque,
    ) {
        let res = delivery_result.as_ref();
        match res {
            Ok(msg) => {
                let key: &str = msg.key_view().unwrap().unwrap();
                debug!(
                    "produced message with key {} in offset {} of partition {}",
                    key,
                    msg.offset(),
                    msg.partition()
                )
            }
            Err(e) => {
                let key: &str = e.1.key_view().unwrap().unwrap();
                warn!("failed to produce message with key {} - {}", key, e.0);
            }
        }
    }
}
