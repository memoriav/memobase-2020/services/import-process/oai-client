mod configuration;
/// Helper functions for working with dates and date ranges
mod date_utils;
/// Helper functions for starting deletion processes for documents in Memobase
mod delete;
mod ff_handler;
/// Helper functions for working with Kafka
mod kafka;
mod mapping;
/// Helper structs and function for report generation
mod report;

use anyhow::{bail, Context, Result};
use clap::Parser;
use configuration::Configuration;
use kafka::CollectionCtxMetadata;
use log::{debug, error, info};
use oai::{list_records::Params as OaiListParams, Client as OaiClient};

use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::path::Path;

use crate::mapping::CollectionProperties;

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::new().filter_or("LOG_LEVEL", "info"))
        .format(|buf, record| {
            writeln!(
                buf,
                "[{}] [oai-client:{}:{}] {}",
                record.level(),
                record.file().unwrap_or("unknown file"),
                record.line().unwrap_or(0),
                record.args()
            )
        })
        .init();
    debug!("Env logger successfully set up");
    let cli_args = configuration::Cli::parse();
    let (from_date, until_date) = configuration::get_from_and_until_date(&cli_args);
    // This is a hardcoded set of identifiers which should be processed from F+F collection
    let accepted_ff_identifiers = vec!["B-05", "B-09", "B-10", "B-12", "B-13"];
    debug!(
        "Set date range: {} - {}",
        from_date
            .map(|v| v.format("%FT%TZ").to_string())
            .unwrap_or_else(|| "<undefined>".to_string()),
        until_date
            .map(|v| v.format("%FT%TZ").to_string())
            .unwrap_or_else(|| "<undefined>".to_string())
    );
    let session_id = if cli_args.session_id.is_empty() {
        format!("{}", chrono::Local::now().format("%Y%m%d%H%M%S"))
    } else {
        cli_args.session_id
    };
    info!("Reading configuration values from environment variables");
    let config = configuration::Configuration::parse()
        .context("Configuration environment variables parsing failed")?;
    info!("Configuration values OK");

    let mut ctx_metadata = CollectionCtxMetadata::build_outside_collection(
        &session_id,
        &config.reporting_step_name,
        &config.app_version,
    );

    debug!("Starting Kafka producer for reports");
    let report_producer = kafka::CustomProducer::new(&config.kafka_reports_topic, &config)
        .context("Couldn't start Kafka producer for reports")?;

    info!("Reading mapping file from {}", &config.mapping_file_path);
    let mut mapping = match mapping::parse(&config.mapping_file_path) {
        Ok(m) => m,
        Err(e) => {
            let msg = format!("There are errors in the mapping: {}", e);
            report_error(None, &ctx_metadata, &msg, &report_producer)?;
            bail!(msg)
        }
    };

    if !cli_args.id.is_empty() {
        mapping = mapping
            .into_iter()
            .filter(|c| cli_args.id.contains(&c.1))
            .filter(|c| !c.0.ignore)
            .collect::<Vec<(CollectionProperties, String)>>();
    }

    if mapping.is_empty() {
        bail!("No collections for given Memobase IDs found! Finishing")
    }

    info!(
        "Will fetch the following collections: {}",
        mapping
            .iter()
            .map(|c| c.1.to_owned())
            .fold(vec![], |mut acc, x| {
                if !acc.contains(&x) {
                    acc.push(x);
                }
                acc
            })
            .join(", ")
    );

    debug!("Starting Kafka producer for documents");
    let document_producer = kafka::CustomProducer::new(&config.kafka_output_topic, &config)
        .context("Couldn't start Kafka producer for documents")?;

    let mut counter = Counter::default();
    let mut delete_process = delete::DeleteProcess::new(
        &config.import_api_host,
        &config.import_api_username,
        &config.import_api_password,
    )
    .context("Creation of delete process failed")
    .map_err(|e| report_error(None, &ctx_metadata, &e.to_string(), &report_producer))
    .unwrap();

    for (oai_params, collection_id) in mapping.into_iter() {
        ctx_metadata = CollectionCtxMetadata::build_inside_collection(
            &collection_id,
            !cli_args.not_published,
            &session_id,
            oai_params.xml_record_tag.as_deref(),
            oai_params.xml_identifier_field_name.as_deref(),
            &config.reporting_step_name,
            &config.app_version,
        );
        let document_ids = if let Ok(ids) = get_document_ids(&config, &oai_params) {
            ids
        } else {
            let msg = "Couldn't read in document ids. Skipping import of collection.";
            report_error(None, &ctx_metadata, msg, &report_producer)?;
            error!("{}", msg);
            continue;
        };
        let oai_client = match OaiClient::new(oai_params.base_url).context("Creating OAI client") {
            Ok(client) => client,
            Err(e) => {
                let msg = format!("Client creation failed: {}", e);
                report_error(None, &ctx_metadata, &msg, &report_producer)?;
                error!("{}", &msg);
                continue;
            }
        };

        // Use GetRecord instead of ListRecords
        if !document_ids.is_empty() {
            for document_id in document_ids {
                info!("Download document with id {}", document_id);
                let result = oai_client
                    .get_record(&document_id, &oai_params.metadata)
                    .await;
                match result {
                    Err(oai::err::Error::IdDoesNotExist(id)) => {
                        let msg = format!("No document found with id {}", id);
                        info!("{}", &msg);
                        report_error(None, &ctx_metadata, &msg, &report_producer)?;
                    }
                    Err(e) => {
                        error!("{}", e);
                        report_error(None, &ctx_metadata, &e.to_string(), &report_producer)?;
                    }
                    Ok(record) => {
                        let record = record.record;
                        if record.header.deleted {
                            counter.add_one_deleted();
                            debug!(
                                "Marking document {} for deletion",
                                &record.header.identifier
                            );
                            delete_process.add_document(&collection_id, &record.header.identifier);
                        } else {
                            counter.add_one_created();
                            debug!("Importing document {}", &record.header.identifier);
                            send_to_kafka(
                                &record.header.identifier,
                                &record.full_record,
                                &ctx_metadata,
                                record.header.deleted,
                                &document_producer,
                                &report_producer,
                            )
                            .context("Sending document to Kafka")?;
                        }
                    }
                }
            }
        } else {
            let params = OaiListParams {
                from: from_date.or(oai_params.from_date),
                until: until_date.or(oai_params.until_date),
                set: oai_params.set,
                metadata_prefix: oai_params.metadata.clone(),
            };

            info!(
            "Start downloading collection {} with the following params:\n- From: {}\n- Until: {}\n- Set: {}\n- Max documents: {}",
            &collection_id,
            &params.from.map(|d| format!("{}", d.format("%FT%TZ"))).unwrap_or_else(|| "<undefined>".to_string()),
            &params.until.map(|d| format!("{}", d.format("%FT%TZ"))).unwrap_or_else(|| "<undefined>".to_string()),
            &params.set.clone().unwrap_or_else(|| "<undefined>".to_string()), &cli_args.max_documents.map(|n| n.to_string()).unwrap_or("<unlimited>".to_string())
        );

            match oai_params.metadata.as_deref() {
                Some("f+f") => {
                    let mut results = oai_client.list_records(Some(params)).await;
                    loop {
                        match results {
                            Err(oai::err::Error::NoRecordsMatch(
                                ref from,
                                ref until,
                                ref set,
                                ref metadata_prefix,
                            )) => {
                                let msg = format!("No documents found with these search parameters:\n- from: {}\n- until: {}\n- set: {}\n- metadata prefix: {}", from, until, set, metadata_prefix);
                                info!("{}", &msg);

                                report_error(None, &ctx_metadata, &msg, &report_producer)?;
                                break;
                            }
                            Err(ref e) => {
                                error!("{}", e);
                                report_error(
                                    None,
                                    &ctx_metadata,
                                    &e.to_string(),
                                    &report_producer,
                                )?;
                                break;
                            }
                            Ok(r) => {
                                let mut max_doc_no_reached = false;
                                for oai_record in &r.records {
                                    if let Ok(items) = ff_handler::split(
                                        &oai_record.full_record,
                                        &accepted_ff_identifiers,
                                    ) {
                                        for item in items {
                                            if item.1.is_none() {
                                                error!("Document has no identifier. Skipping!");
                                                continue;
                                            }
                                            let id = item.1.unwrap();
                                            if oai_record.header.deleted {
                                                counter.add_one_deleted();
                                                debug!("Marking document {} for deletion", &id,);
                                                delete_process.add_document(&collection_id, &id);
                                            } else {
                                                counter.add_one_created();
                                                debug!("Importing document {}", &id,);
                                                send_to_kafka(
                                                    &id,
                                                    &item.0,
                                                    &ctx_metadata,
                                                    false,
                                                    &document_producer,
                                                    &report_producer,
                                                )
                                                .context("Sending document to Kafka")?;
                                            }
                                            if let Some(max_docs) = cli_args.max_documents {
                                                if counter.get_total() >= max_docs {
                                                    info!("Document limit ({}) reached.", max_docs);
                                                    max_doc_no_reached = true;
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        error!("Splitting messages failed!");
                                    }
                                }
                                if r.has_next() && !max_doc_no_reached {
                                    info!("More results found for collection {}", &collection_id);
                                    results = r.get_next(&oai_client).await;
                                } else {
                                    info!(
                                        "No more results found for collection {}",
                                        &collection_id
                                    );
                                    break;
                                }
                            }
                        }
                    }
                    info!("Processing of collection {} finished. {} documents processed ({} created, {} deleted)", &collection_id, counter.get_current_collection(), counter.get_current_collection_created(), counter.get_current_collection_deleted());
                    if delete_process.deletions_queued() {
                        debug!("Sending IDs of deleted documents for removal in Memobase");
                        let res = delete_process
                            .delete_documents()
                            .await
                            .context("Sending of IDs of deleted documents to Import API failed")?;
                        if res.status().is_success() || res.status().is_redirection() {
                            debug!("Request sent with status {}", res.status());
                        } else {
                            error!(
                                "Request for deletion on {} failed with status {}",
                                res.url(),
                                res.status()
                            );
                        }
                    }
                    counter.reset_collection();
                }
                _ => {
                    let mut results = oai_client.list_records(Some(params)).await;
                    loop {
                        match results {
                            Err(oai::err::Error::NoRecordsMatch(
                                from,
                                until,
                                set,
                                metadata_prefix,
                            )) => {
                                info!("No documents found with these search parameters:\n- from: {}\n- until: {}\n- set: {}\n- metadata prefix: {}", from, until, set, metadata_prefix);
                                break;
                            }
                            Err(e) => {
                                error!("{}", e);
                                break;
                            }
                            Ok(r) => {
                                let mut max_doc_no_reached = false;
                                for record in &r.records {
                                    if record.header.deleted {
                                        counter.add_one_deleted();
                                        debug!(
                                            "Marking document {} for deletion",
                                            &record.header.identifier
                                        );
                                        delete_process.add_document(
                                            &collection_id,
                                            &record.header.identifier,
                                        );
                                    } else {
                                        counter.add_one_created();
                                        debug!("Importing document {}", &record.header.identifier,);
                                        send_to_kafka(
                                            &record.header.identifier,
                                            &record.full_record,
                                            &ctx_metadata,
                                            record.header.deleted,
                                            &document_producer,
                                            &report_producer,
                                        )
                                        .context("Sending document to Kafka")?;
                                    }
                                    if let Some(max_docs) = cli_args.max_documents {
                                        if counter.get_total() >= max_docs {
                                            info!("Document limit ({}) reached.", max_docs);
                                            max_doc_no_reached = true;
                                            break;
                                        }
                                    }
                                }
                                if r.has_next() && !max_doc_no_reached {
                                    info!("More results found for collection {}", &collection_id);
                                    results = r.get_next(&oai_client).await;
                                } else {
                                    info!(
                                        "No more results found for collection {}",
                                        &collection_id
                                    );
                                    break;
                                }
                            }
                        }
                    }
                    info!("Processing of collection {} finished. {} documents processed ({} created, {} deleted)", &collection_id, counter.get_current_collection(), counter.get_current_collection_created(), counter.get_current_collection_deleted());
                    if delete_process.deletions_queued() {
                        debug!("Sending IDs of deleted documents for removal in Memobase");
                        let res = delete_process
                            .delete_documents()
                            .await
                            .context("Sending of IDs of deleted documents to Import API failed")?;
                        if res.status().is_success() || res.status().is_redirection() {
                            debug!("Request sent with status {}", res.status());
                        } else {
                            error!(
                                "Request for deletion on {} failed with status {}",
                                res.url(),
                                res.status()
                            );
                        }
                    }
                    counter.reset_collection();
                }
            }
        }
    }
    info!(
        "All finished. {} documents processed in total ({} created, {} deleted).",
        counter.get_total(),
        counter.get_total_created(),
        counter.get_total_deleted()
    );
    Ok(())
}
fn get_document_ids(
    config: &Configuration,
    oai_params: &CollectionProperties,
) -> Result<Vec<String>> {
    let mut document_ids = if let Some(ref list_name) = oai_params.document_list {
        let path = Path::new(&config.document_list_path).join(list_name);
        if !path.exists() {
            bail!("{} does not exist!", path.display());
        }
        let buf_reader = BufReader::new(File::open(path)?);
        buf_reader
            .lines()
            .map_while(Result::ok)
            .collect::<Vec<String>>()
    } else {
        vec![]
    };
    if let Some(ref id) = oai_params.document {
        document_ids.push(id.to_owned())
    };
    Ok(document_ids)
}

fn report_error(
    id: Option<&str>,
    ctx_metadata: &CollectionCtxMetadata,
    payload: &str,
    report_producer: &kafka::CustomProducer,
) -> Result<()> {
    let mut report_builder = report::ReportBuilder::new(
        id.unwrap_or("xxx"),
        ctx_metadata.step_name,
        ctx_metadata.step_version,
        false,
    );
    report_builder.set_message(payload);
    report_builder.set_status(report::ReportStatus::Fatal);
    let report = report_builder.build()?;
    report_producer.send(&report, "xxx", ctx_metadata.build_report_headers())
}

fn send_to_kafka(
    id: &str,
    document: &str,
    collection_metadata: &CollectionCtxMetadata,
    is_deleted: bool,
    document_producer: &kafka::CustomProducer,
    report_producer: &kafka::CustomProducer,
) -> Result<()> {
    let mut report_builder = report::ReportBuilder::new(
        id,
        collection_metadata.step_name,
        collection_metadata.step_version,
        is_deleted,
    );

    let sent = document_producer.send(document, id, collection_metadata.build_document_headers());

    if let Err(e) = sent {
        report_builder.set_status(report::ReportStatus::Fatal);
        report_builder.set_message("Couldn't send document to Kafka");
        error!(
            "Error while sending document {} happened: {}",
            id,
            e.to_string()
        );
    }

    let report = report_builder.build()?;
    let reported = report_producer.send(&report, id, collection_metadata.build_report_headers());
    if let Err(e) = reported {
        bail!(
            "Error while sending report for document {} happened: {}",
            id,
            e.to_string()
        );
    }
    Ok(())
}

#[derive(Default)]
struct Counter {
    total_created: usize,
    total_deleted: usize,
    current_collection_created: usize,
    current_collection_deleted: usize,
}

impl Counter {
    pub fn add_one_created(&mut self) {
        self.total_created += 1;
        self.current_collection_created += 1;
    }

    pub fn add_one_deleted(&mut self) {
        self.total_deleted += 1;
        self.current_collection_deleted += 1;
    }

    pub fn reset_collection(&mut self) {
        self.current_collection_created = 0;
        self.current_collection_deleted = 0;
    }

    pub fn get_total_created(&self) -> usize {
        self.total_created
    }

    pub fn get_total_deleted(&self) -> usize {
        self.total_deleted
    }

    pub fn get_total(&self) -> usize {
        self.total_created + self.total_deleted
    }

    pub fn get_current_collection_created(&self) -> usize {
        self.current_collection_created
    }

    pub fn get_current_collection_deleted(&self) -> usize {
        self.current_collection_deleted
    }

    pub fn get_current_collection(&self) -> usize {
        self.current_collection_created + self.current_collection_deleted
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn counter() {
        let mut counter = Counter::default();
        assert_eq!(counter.get_total(), 0);
        assert_eq!(counter.get_current_collection(), 0);
        counter.add_one_created();
        assert_eq!(counter.get_total(), 1);
        assert_eq!(counter.get_current_collection(), 1);
        counter.reset_collection();
        assert_eq!(counter.get_total(), 1);
        assert_eq!(counter.get_current_collection(), 0);
    }
}
