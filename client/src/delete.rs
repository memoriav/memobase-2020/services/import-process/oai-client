use anyhow::{Context, Result};
use rand::distributions::Uniform;
use rand::prelude::*;
use reqwest::{Client, Response};
use serde::Serialize;

#[derive(Serialize)]
struct RequestBody {
    ids: Vec<String>,
    recursive_delete: bool,
}

pub struct DeleteProcess<'a> {
    client: Client,
    host: &'a str,
    auth_user: &'a str,
    auth_password: &'a str,
    documents: Vec<String>,
}

impl<'a> DeleteProcess<'a> {
    pub fn new(
        host: &'a str,
        auth_user: &'a str,
        auth_password: &'a str,
    ) -> Result<DeleteProcess<'a>> {
        let client = Client::new();
        let documents = vec![];
        Ok(DeleteProcess {
            client,
            host,
            auth_user,
            auth_password,
            documents,
        })
    }

    pub fn add_document(&mut self, record_set_id: &str, original_id: &str) {
        self.documents
            .push(format!("orig:{}:{}", record_set_id, original_id));
    }

    pub fn deletions_queued(&self) -> bool {
        !self.documents.is_empty()
    }

    pub async fn delete_documents(&mut self) -> Result<Response> {
        let body = RequestBody {
            ids: self.documents.drain(..).collect(),
            recursive_delete: false,
        };
        let session_id: String = thread_rng()
            .sample_iter(Uniform::new(char::from(32), char::from(126)))
            .take(8)
            .map(char::from)
            .collect();
        let host = format!(
            "{}{}v2/drupal/delete/{}/false",
            self.host,
            if self.host.ends_with('/') { "" } else { "/" },
            session_id
        );
        self.client
            .post(host)
            .basic_auth(self.auth_user, Some(self.auth_password))
            .json(&body)
            .send()
            .await
            .context("request failed")
    }
}
