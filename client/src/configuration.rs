use anyhow::{Context, Result};
use chrono::{DateTime, Utc};
use clap::{ArgGroup, Parser};

use std::env;

use crate::date_utils::*;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[command(group(ArgGroup::new("fixed_timespan")
                .required(false)
                .multiple(true)
                .conflicts_with("dyn_timespan")
                .args(["from_date", "until_date"]),
        ))]
#[command(group(ArgGroup::new("dyn_timespan")
                .required(false)
                .multiple(false)
                .conflicts_with("fixed_timespan")
                .args(["last_month", "last_week", "last_year"]),
        ))]
pub struct Cli {
    /// Memobase collections to fetch. Leave empty to download all defined in the mapping
    pub id: Vec<String>,
    /// Include documents from <YYYY-MM-DD>
    #[clap(long, value_parser = to_from_date)]
    pub from_date: Option<DateTime<Utc>>,
    /// Include documents until <YYYY-MM-DD>
    #[clap(long, value_parser = to_until_date)]
    pub until_date: Option<DateTime<Utc>>,
    /// Get documents modified last week
    #[clap(long)]
    pub last_week: bool,
    /// Get documents modified last month
    #[clap(long)]
    pub last_month: bool,
    /// Get documents modified last year
    #[clap(long)]
    pub last_year: bool,
    /// Stop harvesting after <MAX_DOCUMENTS> documents
    #[clap(long)]
    pub max_documents: Option<usize>,
    /// Import documents but do not publish
    #[clap(long)]
    pub not_published: bool,
    /// ID for unequivocally identify download session
    #[clap(long, required = false, default_value = "")]
    pub session_id: String,
}

fn to_from_date(s: &str) -> Result<DateTime<Utc>> {
    string_to_date(s, false)
}

fn to_until_date(s: &str) -> Result<DateTime<Utc>> {
    string_to_date(s, true)
}

pub struct Configuration {
    /// Path to mapping file
    pub mapping_file_path: String,
    /// Path to document lists directory
    pub document_list_path: String,
    /// `host:port` of Kafka brokers
    pub kafka_bootstrap_servers: String,
    /// Kafka topic name where the processed documents are written to
    pub kafka_output_topic: String,
    /// Kafka topic name where reports are written to
    pub kafka_reports_topic: String,
    /// Custom Kafka producer settings: `key1=value1,key2=value2,...`
    pub kafka_producer_configurations: Vec<String>,
    /// Protocol used to communicate with brokers. Valid values are: `plaintext`, `ssl`, `sasl_plaintext`, `sasl_ssl`
    pub kafka_security_protocol: String,
    /// File or directory path to CA certificate(s) for verifying the broker's key
    pub kafka_ssl_ca_location: String,
    /// Path to client's private key (PEM) used for authentication
    pub kafka_ssl_key_location: String,
    /// Path to client's public key (PEM) used for authentication
    pub kafka_ssl_certificate_location: String,
    /// Enable OpenSSL's builtin broker (server) certificate verification. `true` or `false`
    pub kafka_enable_ssl_certificate_verification: String,
    /// Version number of application used in reports
    pub app_version: String,
    /// Identifier for service in reports
    pub reporting_step_name: String,
    /// Host name of the Import API
    pub import_api_host: String,
    /// User name of the Import API basic authentication
    pub import_api_username: String,
    /// Password of the Import API basic authentication
    pub import_api_password: String,
}

impl Configuration {
    /// Parse and check values from environment variables.
    pub fn parse() -> Result<Configuration> {
        let mapping_file_path = env::var("MAPPING_FILE_PATH")
            .context("Missing environment variable: MAPPING_FILE_PATH")?;
        let document_list_path = env::var("DOCUMENT_LIST_PATH")
            .context("Missing environment variable: DOCUMENT_LIST_PATH")?;
        let kafka_bootstrap_servers = env::var("KAFKA_BOOTSTRAP_SERVERS")
            .context("Missing environment variable: KAFKA_BOOTSTRAP_SERVERS")?;
        let kafka_output_topic = env::var("KAFKA_OUTPUT_TOPIC")
            .context("Missing environment variable: KAFKA_OUTPUT_TOPIC")?;
        let kafka_reports_topic = env::var("KAFKA_REPORTS_TOPIC")
            .context("Missing environment variable: KAFKA_REPORTS_TOPIC")?;
        let kafka_producer_configurations = env::var("KAFKA_PRODUCER_CONFIGURATIONS")
            .map(|v| {
                v.split(',')
                    .filter(|e| !e.is_empty())
                    .map(|e| e.to_owned())
                    .collect::<Vec<String>>()
            })
            .context("Missing environment variable: KAFKA_PRODUCER_CONFIGURATIONS")?;
        let kafka_security_protocol = env::var("KAFKA_SECURITY_PROTOCOL")
            .context("Missing environment variable: KAFKA_SECURITY_PROTOCOL")?;
        let kafka_ssl_ca_location = env::var("KAFKA_SSL_CA_LOCATION")
            .context("Missing environment variable: KAFKA_SSL_CA_LOCATION")?;
        let kafka_ssl_key_location = env::var("KAFKA_SSL_KEY_LOCATION")
            .context("Missing environment variable: KAFKA_SSL_KEY_LOCATION")?;
        let kafka_ssl_certificate_location = env::var("KAFKA_SSL_CERTIFICATE_LOCATION")
            .context("Missing environment variable: KAFKA_SSL_CERTIFICATE_LOCATION")?;
        let kafka_enable_ssl_certificate_verification = env::var(
            "KAFKA_ENABLE_SSL_CERTIFICATE_VERIFICATION",
        )
        .context("Missing environment variable: KAFKA_ENABLE_SSL_CERTIFICATE_VERIFICATION")?;
        let app_version =
            env::var("APP_VERSION").context("Missing environment variable: APP_VERSION")?;
        let reporting_step_name = env::var("REPORTING_STEP_NAME")
            .context("Missing environment variable: REPORTING_STEP_NAME")?;
        let import_api_host =
            env::var("IMPORT_API_HOST").context("Missing environment variable: IMPORT_API_HOST")?;
        let import_api_username = env::var("IMPORT_API_USERNAME")
            .context("Missing environment variable: IMPORT_API_USERNAME")?;
        let import_api_password = env::var("IMPORT_API_PASSWORD")
            .context("Missing environment variable: IMPORT_API_PASSWORD")?;
        Ok(Configuration {
            mapping_file_path,
            document_list_path,
            kafka_bootstrap_servers,
            kafka_output_topic,
            kafka_reports_topic,
            kafka_producer_configurations,
            kafka_security_protocol,
            kafka_ssl_ca_location,
            kafka_ssl_key_location,
            kafka_ssl_certificate_location,
            kafka_enable_ssl_certificate_verification,
            app_version,
            reporting_step_name,
            import_api_host,
            import_api_username,
            import_api_password,
        })
    }
}

pub fn get_from_and_until_date(args: &Cli) -> (Option<DateTime<Utc>>, Option<DateTime<Utc>>) {
    let now = Utc::now();
    if args.from_date.is_some() || args.until_date.is_some() {
        (args.from_date, args.until_date)
    } else if args.last_week {
        let range = compute_previous_week(now);
        (Some(range.0), Some(range.1))
    } else if args.last_month {
        let range = compute_previous_month(now);
        (Some(range.0), Some(range.1))
    } else if args.last_year {
        let range = compute_previous_year(now);
        (Some(range.0), Some(range.1))
    } else {
        (None, None)
    }
}
