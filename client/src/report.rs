use anyhow::{Context, Result};
use chrono::{Local, NaiveDateTime};
use serde::{Serialize, Serializer};

/// Informs about the (un-)successful attempt to process a document by a service
#[derive(Serialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum ReportStatus {
    Success,
    Fatal,
}

/// Helps building a report
pub struct ReportBuilder {
    id: String,
    step_name: String,
    step_version: String,
    message: String,
    status: ReportStatus,
}

impl ReportBuilder {
    pub fn new(document_id: &str, step_name: &str, step_version: &str, is_deleted: bool) -> Self {
        ReportBuilder {
            id: document_id.to_string(),
            step_name: step_name.to_string(),
            step_version: step_version.to_string(),
            message: format!(
                "{} document successfully processed",
                if is_deleted { "Deleted" } else { "New" }
            ),
            status: ReportStatus::Success,
        }
    }

    pub fn set_message(&mut self, message: &str) {
        self.message = message.to_string();
    }

    pub fn set_status(&mut self, status: ReportStatus) {
        self.status = status;
    }

    pub fn build(self) -> Result<String> {
        let now = Local::now();
        let report = Report {
            id: self.id,
            step: self.step_name,
            step_version: self.step_version,
            // TODO: Probably change this to local timestamp
            timestamp: now.naive_local(),
            status: self.status,
            message: self.message,
        };

        serde_json::to_string(&report).context("Parsing failed!")
    }
}

/// Represents the outcome of the document's processing by a service. Aggregated, reports helps to
/// identify problems in one of Memobase's process chain (e.g. the import workflow).
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Report {
    /// Document identifier
    pub id: String,
    /// Name of the service generating the record
    pub step: String,
    /// Application version
    pub step_version: String,
    /// Date and time when the report has been generated
    #[serde(serialize_with = "serialize_date_time")]
    pub timestamp: NaiveDateTime,
    /// Processing status
    pub status: ReportStatus,
    /// Processing message
    pub message: String,
}

const DATE_FORMAT: &str = "%FT%T%.3f";

pub fn serialize_date_time<S>(date: &NaiveDateTime, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = format!("{}", date.format(DATE_FORMAT));
    serializer.serialize_str(&s)
}
