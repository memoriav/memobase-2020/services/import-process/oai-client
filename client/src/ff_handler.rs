use anyhow::Result;
use roxmltree::{Document, Node};
use std::str;

pub fn split(body: &str, collection_ids: &[&str]) -> Result<Vec<(String, Option<String>)>> {
    let xml_document = Document::parse(body)?;
    let root = xml_document.root_element();
    let items = root
        .children()
        .filter(|c| c.has_tag_name("archdesc"))
        .flat_map(|c| c.children())
        .filter(|c| c.has_tag_name("dsc"))
        .flat_map(|c| c.children())
        .filter(|c| c.has_tag_name("c") && c.attribute("level").filter(|v| *v == "file").is_some())
        .filter(|n| {
            get_id_from_c_node(n)
                .filter(|id| collection_ids.contains(id))
                .is_some()
        })
        .flat_map(|c| c.children())
        .filter(|c| c.has_tag_name("c") && c.attribute("level").filter(|v| *v == "item").is_some());

    let mut result_set = vec![];
    for item in items {
        let id = get_id_from_c_node(&item).map(|id| id.to_owned());
        let item_range = item.range();
        let document_content = item.document().input_text().bytes();
        let child_node_content = document_content
            .skip(item_range.start)
            .take(item_range.end - item_range.start)
            .collect::<Vec<u8>>();
        result_set.push((str::from_utf8(&child_node_content)?.to_string(), id));
    }
    Ok(result_set)
}

fn get_id_from_c_node<'a>(file_node: &'a Node) -> Option<&'a str> {
    file_node
        .children()
        .filter(|c| c.has_tag_name("did"))
        .flat_map(|c| c.children())
        .filter(|c| c.has_tag_name("unitid"))
        .flat_map(|n| n.text())
        .next()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_split_document() {
        let collection_ids = vec!["B-01"];
        let document = r#"<ead><archdesc level="fonds" relatedencoding="ISAD(G)v2"><did><unittitle encodinganalog="3.1.2">Archiv F+F Schule</unittitle><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B</unitid></did><dsc type="combined"><c level="file"><did><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-01</unitid></did><c level="item"><did><unittitle encodinganalog="3.1.2">Infodossier F+F Schule</unittitle><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-01-01</unitid></did></c><c level="item"><did><unittitle encodinganalog="3.1.2">Einladung Gedankenaustausch</unittitle><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-01-02</unitid></did></c></c><c level="file"><did><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-02</unitid></did><c level="item"><did><unittitle encodinganalog="3.1.2">Infodossier F+F Schule</unittitle><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-02-01</unitid></did></c></c></dsc></archdesc></ead>"#;
        let items = split(document, &collection_ids)
            .unwrap()
            .into_iter()
            .map(|t| t.0)
            .collect::<Vec<String>>();
        let expected_result = vec![
            r#"<c level="item"><did><unittitle encodinganalog="3.1.2">Infodossier F+F Schule</unittitle><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-01-01</unitid></did></c>"#.to_string(),
            r#"<c level="item"><did><unittitle encodinganalog="3.1.2">Einladung Gedankenaustausch</unittitle><unitid encodinganalog="3.1.1" countrycode="CH" repositorycode="FF1971-D">B-01-02</unitid></did></c>"#.to_string(),
        ];
        assert_eq!(items, expected_result)
    }
}
